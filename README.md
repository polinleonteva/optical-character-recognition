Optical Character Recognition
==============================

Необходимо реализовать архитектуру CRNN из статьи (references\1507.05717.pdf). В качестве генератора датасета рекомендуется использовать файл репозитория make_dataset.py

Project Organization
------------

    ├── README.md          <- The top-level README for developers using this project.

    │
    ├── references         <- Articles and docs.
    │
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    └── src                <- Source code for use in this project.
        ├── __init__.py    <- Makes src a Python module
        │
        ├── data           <- Script generate data
        │   └── make_dataset.py
        │
        │
        └── models         <- Scripts to train models and then use trained models to make
            │                 predictions
            ├── CRNN.py
            ├── predict_model.py
            └── train.py