import os

import cv2
import torch
from torchvision.transforms import ToTensor, Compose, Normalize
import torch.utils.data as data_utils
from src.models.CRNN import CRNN_model
from src.data.make_dataset import CapchaDataset
from itertools import groupby
import matplotlib as mpl
import matplotlib.pyplot as plt


size_of_one_digit = 32
img_chanel = 1


def predict(model_path, number_of_test_imgs: int = 10, device=torch.device('cuda')):
    demo_ds = CapchaDataset((4, 5), samples=100)
    model = CRNN_model(img_chanel, size_of_one_digit,
                       size_of_one_digit * 5, demo_ds.num_classes).to(device)
    model.load_state_dict(
        torch.load(model_path,
                   map_location=device)
    )
    model.eval()
    test_loader = data_utils.DataLoader(demo_ds, batch_size=number_of_test_imgs)
    test_preds = []
    (x_test, y_test) = next(iter(test_loader))
    y_pred = model(
        x_test.view(x_test.shape[0], 1, x_test.shape[1], x_test.shape[2]).cuda()
    )
    _, max_index = torch.max(y_pred, dim=2)
    for i in range(x_test.shape[0]):
        raw_prediction = list(max_index[:, i].detach().cpu().numpy())
        prediction = torch.IntTensor(
            [c for c, _ in groupby(raw_prediction) if c != demo_ds.blank_label]
        )
        prediction = prediction.to(torch.int32)
        prediction = prediction[prediction != demo_ds.blank_label].tolist()
        str_prediction = ' '.join(map(str, prediction))
        test_preds.append(str_prediction)

    for j in range(len(x_test)):
        mpl.rcParams["font.size"] = 8
        plt.imshow(x_test[j], cmap="gray")
        mpl.rcParams["font.size"] = 18
        str_y_true = y_test[j].to(torch.int32)
        str_y_true = str_y_true[str_y_true != demo_ds.blank_label].tolist()
        str_y_true = ' '.join(map(str, str_y_true))
        plt.gcf().text(x=0.1, y=0.1, s="     Actual: " + str_y_true)
        plt.gcf().text(x=0.1, y=0.2, s="Predicted: " + test_preds[j])
        plt.savefig(f"D:\work\Optical_Char_Rec\data\processed\plot_{j}.png")
        plt.clf()



if __name__ == "__main__":
    data_folder = r'D:\work\Optical_Char_Rec\data\processed'
    print(predict(r'D:\work\Optical_Char_Rec\models\checkpoint_5_train2.pt'))
