from torch import nn


class ConvLayers(nn.Module):
    def __init__(
            self,
            input_ch,
            out_ch=512
    ):
        """
        Backbone layers for CRNN based on the VGG-VeryDeep architecture.

        Args:
            input_ch (int): Input size of the image.
            out_ch (int): Number of output channels. Default is 512.
        """
        super(ConvLayers, self).__init__()

        self.output = [int(out_ch / 8), int(out_ch / 4),
                       int(out_ch / 2), out_ch]  # [64, 128, 256, 512]

        self.conv_1 = nn.Sequential(
            nn.Conv2d(in_channels=input_ch, out_channels=self.output[0], kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )

        self.conv_2 = nn.Sequential(
            nn.Conv2d(in_channels=self.output[0], out_channels=self.output[1], kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )

        self.conv_3 = nn.Sequential(
            nn.Conv2d(in_channels=self.output[1], out_channels=self.output[2], kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True)
        )

        self.conv_4 = nn.Sequential(
            nn.Conv2d(in_channels=self.output[2], out_channels=self.output[2], kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d((2, 1))
        )

        self.conv_bn_1 = nn.Sequential(
            nn.Conv2d(in_channels=self.output[2], out_channels=self.output[3], kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(inplace=True),
        )

        self.conv_bn_2 = nn.Sequential(
            nn.Conv2d(in_channels=self.output[3], out_channels=self.output[3], kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d((2, 1)),
        )

        self.conv_5 = nn.Sequential(
            nn.Conv2d(in_channels=self.output[3], out_channels=self.output[3], kernel_size=2, stride=1, padding=0),
            nn.ReLU(inplace=True)
        )

    def _forward_impl(self, x):
        # This exists since TorchScript doesn't support inheritance, so the superclass method
        # (this one) needs to have a name other than `forward` that can be accessed in a subclass
        l_1 = self.conv_1(x)
        l_2 = self.conv_2(l_1)
        l_3 = self.conv_3(l_2)
        l_4 = self.conv_4(l_3)
        l_5 = self.conv_bn_1(l_4)
        l_6 = self.conv_bn_2(l_5)
        output = self.conv_5(l_6)
        return output

    def forward(self, x):
        return self._forward_impl(x)


class BidirectionalLSTM(nn.Module):
    """
            Bidirectional LSTM module.

            Args:
                input_size (int): Size of the input.
                hidden_units (int): Number of hidden units in the LSTM.

            Input:
                - x (Tensor): Input tensor of shape (batch_size, seq_length, input_size).

            Output:
                - recurrent (Tensor): Output tensor of shape (batch_size, seq_length, 2 * hidden_units).
            """

    def __init__(self, input_size, hidden_units):
        super(BidirectionalLSTM, self).__init__()

        self.rnn = nn.LSTM(input_size, hidden_units, bidirectional=True, batch_first=True)

    def forward(self, x):
        recurrent, _ = self.rnn(x)  # batch_size x T x input_size -> batch_size x T x (2*hidden_size)
        return recurrent


class CRNN_model(nn.Module):
    """
           CRNN model for text recognition.

           Args:
               channels (int): Number of input channels (must be 1, as in article).
               img_h (int): Height of the input image.
               alphabet_num (int): Number of characters in the pull of digits.
               map_to_seq_hidden (int): Number of hidden units in the linear layer for mapping to sequence.
               hidden_units (int): Number of hidden units in the BidirectionalLSTM layers.
           """

    def __init__(self, channels, img_h, alphabet_num,  map_to_seq_hidden=64, hidden_units=256):
        super(CRNN_model, self).__init__()
        self.in_channels = channels
        self.output_ch = 512

        # CNN feature extractor
        self.CNN_extractor = ConvLayers(self.in_channels, self.output_ch)

        # Linear layer for mapping to sequence
        self.map_to_seq = nn.Linear(512 * (img_h // 16 - 1), map_to_seq_hidden)

        # Sequence mapping layers (Bidirectional LSTMs)
        self.Sequence_map = nn.Sequential(
            BidirectionalLSTM(map_to_seq_hidden, hidden_units),
            BidirectionalLSTM(2*hidden_units, hidden_units),
        )

        # Output layer for recognition
        self.recognition = nn.Linear(2*hidden_units, alphabet_num)

    def forward(self, img_batch):
        # CNN feature extraction
        features = self.CNN_extractor(img_batch)
        batch, channel, height, width = features.size()

        # Reshape features for sequence mapping
        features = features.view(batch, channel * height, width)
        features = features.permute(2, 0, 1)  # (width, batch, feature)

        # Map features to sequence
        seq = self.map_to_seq(features)

        # Apply sequence mapping layers
        seq = self.Sequence_map(seq)

        output = self.recognition(seq)
        return output
