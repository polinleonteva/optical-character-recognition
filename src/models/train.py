import sys

import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torchmetrics import CharErrorRate

from src.models.CRNN import CRNN_model
from src.data.make_dataset import CapchaDataset

from tqdm import tqdm
import numpy as np
from itertools import groupby
from colorama import Fore

cer_metric = CharErrorRate()


def compute_cer(predicted, labels, batch_size, blank):
    """
       Compute the Character Error Rate (CER) metric for predicted sequences compared to target labels.

       Args:
           predicted (torch.Tensor): Predicted sequences of shape (sequence_length, batch_size).
           labels (torch.Tensor): Target label sequences of shape (batch_size, sequence_length).
           batch_size (int): Number of sequences in a batch.
           blank (int): Index representing the blank symbol in the predicted sequences.

       Returns:
           float: Average CER across the batch, rounded to four decimal places.
       """

    batch_cer = []
    for i in range(batch_size):
        # Extract the predicted sequence for the current index i
        raw_prediction = list(
            predicted[:, i].detach().cpu().numpy()
        )

        # Remove consecutive duplicates and the blank symbol from the predicted sequence
        prediction = torch.IntTensor(
            [c for c, _ in groupby(raw_prediction) if c != blank]
        )
        prediction = prediction.to(torch.int32)
        prediction = prediction[prediction != blank].tolist()
        str_prediction = ''.join(map(str, prediction))

        # Convert the target label sequence to string representation
        labels_i = labels[i].to(torch.int32)
        labels_i = labels_i[labels_i != blank].tolist()
        str_y_true = ''.join(map(str, labels_i))

        # Compute the CER for the current predicted sequence and target label sequence
        batch_cer.append(cer_metric(str_prediction, str_y_true).numpy())

    # Compute the average CER across the batch
    return round(np.array(batch_cer).mean(), 4)


def train_model(model, train_loader, criterion, optimizer, device, blank):
    """
        Train the model using the provided data loader, criterion, and optimizer.

        Args:
            model (torch.nn.Module): The model to train.
            train_loader (torch.utils.data.DataLoader): Data loader for training data.
            criterion: The loss criterion used for training.
            optimizer: The optimizer used for updating the model's parameters.
            device: The device on which to perform training.
            blank (int): Index representing the blank symbol.

        Returns:
            tuple: A tuple containing the average loss and CER across the training data.
        """

    model.train()
    total_loss = 0.0
    total_cer = []

    progress_bar = tqdm(train_loader, desc="Training", position=0, leave=True, file=sys.stdout,
                        bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.GREEN, Fore.RESET))

    for images, labels in progress_bar:
        images = images.to(device)
        labels = labels.to(device)

        optimizer.zero_grad()

        # Reshape the input images before passing them through the OCR
        images = images.view(images.shape[0], 1, images.shape[1], images.shape[2])
        outputs = model(images)

        #  The lengths of the input sequences, where each length is the same as the length of the predicted sequences.
        input_lengths = torch.full((images.size(0),), outputs.size(0), dtype=torch.long)
        #  The lengths of the target sequences,
        #  where each length is the same as the length of the individual label sequences.
        target_lengths = torch.full((labels.size(0),), labels.size(1), dtype=torch.long)

        loss = criterion(outputs, labels, input_lengths, target_lengths)
        loss.backward()

        optimizer.step()

        total_loss += loss.item()
        predicted = outputs.argmax(dim=2)

        # Update the CER metric
        cer_now = compute_cer(predicted, labels, images.shape[0], blank)
        total_cer.append(cer_now)
        progress_bar.set_postfix({"Loss": total_loss / (progress_bar.n + 1), "CER": cer_now})

    # Return the computed CER
    return total_loss / len(train_loader), round(np.array(total_cer).mean(), 4)


def test_model(model, test_loader, device, blank):
    model.eval()
    total_cer = []

    progress_bar = tqdm(test_loader, desc="Testing", position=0, leave=True, file=sys.stdout,
                        bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.BLUE, Fore.RESET))

    with torch.no_grad():
        for images, labels in progress_bar:
            images = images.to(device)
            labels = labels.to(device)

            images = images.view(images.shape[0], 1, images.shape[1], images.shape[2])
            outputs = model(images)

            predicted = outputs.argmax(dim=2)
            # Update the CER metric
            cer_now = compute_cer(predicted, labels, images.shape[0], blank)
            total_cer.append(cer_now)

            progress_bar.set_postfix({"CER": cer_now})

    return round(np.array(total_cer).mean(), 4)


def main():
    # Set the device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    num_epochs = 10
    one_dgt_size = 32
    img_channels = 1
    model_save_path = r"D:\work\Optical_Char_Rec\models"

    # Define the datasets and data loaders
    train_dataset = CapchaDataset(seq_len=(4, 5))
    test_dataset = CapchaDataset(seq_len=(4, 5), samples=100)
    train_loader = DataLoader(train_dataset, batch_size=64, shuffle=True)
    test_loader = DataLoader(test_dataset, batch_size=1)

    # Define the model
    model = CRNN_model(
        img_channels, one_dgt_size,
        train_dataset.num_classes
    ).to(device)

    # Define the optimizer and criterion
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
    criterion = nn.CTCLoss(blank=train_dataset.blank_label, reduction="mean", zero_infinity=True)

    # Training loop
    current_cer = 0
    for epoch in range(num_epochs):
        print(f"Epoch {epoch + 1}/{num_epochs}")

        train_loss, train_cer = train_model(model, train_loader, criterion, optimizer, device,
                                            train_dataset.blank_label)
        print(f"Train Loss: {train_loss:.4f}")
        print(f"Train cer: {train_cer:.4f}")

        test_cer = test_model(model, test_loader, device, test_dataset.blank_label)
        print(f"Test CER: {test_cer:.4f}")

        # Save the trained model
        if test_cer < current_cer:
            model_out_name = model_save_path + f"/checkpoint_{epoch}_train3.pt"
            torch.save(model.state_dict(), model_out_name)


if __name__ == "__main__":
    main()
