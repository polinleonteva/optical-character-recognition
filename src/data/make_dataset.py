from typing import Tuple, Union

import matplotlib.pyplot as plt
import numpy as np
import torch
import torchvision.transforms.functional as func
from torch.utils.data import Dataset
from torchvision import datasets, transforms

import torch.nn.functional as F


class CapchaDataset(Dataset):
    """
    Dataset that generates captcha of length 'seq_len' from the EMNIST dataset.
    """

    def __init__(
            self,
            seq_len: Union[int, Tuple[int, int]],
            img_h: int = 32,
            split: str = "digits",
            samples: int = None,
    ):
        """
               Initialize the CaptchaDataset.

               Args:
                   seq_len (int or Tuple[int, int]): Length of the captcha sequence.
                   If int, fixed length is used. If Tuple[int, int], a range of sequence lengths is allowed.

                   img_h (int): Height of the captcha image. Default is 32.

                   split (str): Split of the EMNIST dataset to use. Default is 'digits'.

                   samples (int): Number of samples to generate.
                   If specified, it overrides the total number of possible combinations.
               """

        self.emnist_dataset = datasets.EMNIST(
            r"D:\work\Optical_Char_Rec\data\raw\EMNIST", split=split, train=True, download=True
        )
        self.seq_len = seq_len
        self.blank_label = len(self.emnist_dataset.classes)
        self.img_h = img_h
        self.samples = samples
        self.num_classes = len(self.emnist_dataset.classes) + 1

        if isinstance(seq_len, int):
            self._min_seq_len = seq_len
            self._max_seq_len = seq_len
        elif (
                isinstance(seq_len, Tuple)
                and len(seq_len) == 2
                and isinstance(seq_len[0], int)
        ):
            self._min_seq_len = seq_len[0]
            self._max_seq_len = seq_len[1]

    def __len__(self):
        """
        Returns the number of possible captcha combinations.

        If 'samples' is specified, it returns 'samples' instead of the total number of combinations.
        """

        if self.samples is not None:
            return self.samples
        return len(self.emnist_dataset.classes) ** self._max_seq_len

    def __preprocess(self, random_images: torch.Tensor) -> np.ndarray:
        """
                Preprocesses the random images to form the captcha image.

                Args:
                    random_images (torch.Tensor): Randomly selected images.

                Returns:
                    np.ndarray: Preprocessed captcha image.
                """

        transformed_images = []

        # Resize images to a fixed height
        random_images = F.interpolate(random_images.unsqueeze(0), size=32, mode='bilinear', align_corners=False)
        random_images = random_images.squeeze(0)

        # Apply transformations to each image
        for imgs in random_images:
            imgs = transforms.ToPILImage()(imgs)
            imgs = func.rotate(imgs, -90, fill=[0.0])
            imgs = func.hflip(imgs)
            imgs = transforms.ToTensor()(imgs).numpy()
            transformed_images.append(imgs)

        # Stack and arrange images to form the captcha image
        images = np.array(transformed_images)
        images = np.hstack(
            images.reshape((len(transformed_images), self.img_h, self.img_h))
        )
        full_img = np.zeros(shape=(self.img_h, self._max_seq_len * self.img_h)).astype(
            np.float32
        )
        full_img[:, 0: images.shape[1]] = images
        return full_img

    def __getitem__(self, idx):
        """
                Generates a single captcha image and its label.

                Args:
                    idx (int): Index of the sample.

                Returns:
                    Tuple[np.ndarray, np.ndarray]: Captcha image and its corresponding label.
                """

        # Get random seq_len
        random_seq_len = np.random.randint(self._min_seq_len, self._max_seq_len + 1)
        # Get random ind
        random_indices = np.random.randint(
            len(self.emnist_dataset.data), size=(random_seq_len,)
        )

        # Get random indices and images from the EMNIST dataset
        random_images = self.emnist_dataset.data[random_indices]
        random_digits_labels = self.emnist_dataset.targets[random_indices]

        # Create label tensor with blank labels
        labels = torch.zeros((1, self._max_seq_len))
        labels = torch.fill(labels, self.blank_label)
        labels[0, 0: len(random_digits_labels)] = random_digits_labels

        # Preprocess images and convert labels to numpy array
        x = self.__preprocess(random_images)
        y = labels.numpy().reshape(self._max_seq_len)
        return x, y


if __name__ == "__main__":
    # Generate captcha dataset with 3 to 5 characters
    ds = CapchaDataset((3, 5))
    data_loader = torch.utils.data.DataLoader(ds, batch_size=2)

    # Iterate over the dataset and display captcha images with labels
    for i, (x_batch, y_batch) in enumerate(data_loader):
        print(i)
        for img, label in zip(x_batch, y_batch):
            plt.imshow(img)
            title = [str(n) for n in label.numpy()]
            plt.title("".join(title))
            plt.show()
